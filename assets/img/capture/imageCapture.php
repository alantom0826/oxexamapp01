<?php
		include_once "../../../mainfile.php";
    
		if($_POST["save"] && !empty($_SESSION['XOOPS_TOKEN_SESSION']['qId'])){
			$type = $_POST["type"];
			if($_POST["name"] and ($type=="JPG" or $type=="PNG")){
				$img = base64_decode($_POST["image"]);
				
				if($_SESSION['XOOPS_TOKEN_SESSION']['qId']==-1){
					//put image in temp folder when the user add a new question
					$qFolder = "img/".$_SESSION['XOOPS_TOKEN_SESSION']['tmpFolder'];	
				}else{
					//put image in question folder when the user edit an old question
					$qFolder = "img/".$_SESSION['XOOPS_TOKEN_SESSION']['qId'];	
				}
				
				if(!is_dir($qFolder)){
					mkdir($qFolder,0777);
				}
				
				$myFile = $qFolder."/".$_POST["name"].".".$type ;
				$fh = fopen($myFile, 'w');
				fwrite($fh, $img);
				fclose($fh);
				echo $qFolder."/".$_POST["name"].".".$type;
			}
		}else{
			header('Content-Type: image/jpeg');
			echo base64_decode($_POST["image"]);
		}
?>
