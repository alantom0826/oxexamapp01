package com.ossii.oxexamapp;

import android.provider.BaseColumns;

public interface DbConstants extends BaseColumns {
	
	public static final String SERVER_ADD = "http://alantom.dyndns-free.com/AndroidConnectDB/";
	
	public static final String TABLE_NAME = "papers";
	
	public static final String P_ID = "_id";
	public static final String P_NUM_OF_TIMES = "num_of_times";
	public static final String P_START_AT = "start_at";
	public static final String P_NAME = "name";
	public static final String P_MYSQL_PAPER_ID= "mysql_paper_id	";
	public static final String P_MYSQL_EXAM_ID = "mysql_exam_id	";
	public static final String P_SUBJECT_ID = "subject_id";
	public static final String P_END_AT = "end_at";
	public static final String P_PARTS = "parts";
	public static final String P_DURATION = "duration";
	public static final String P_DESC = "desc";
	public static final String P_EXPIRE = "expire";

	
}
