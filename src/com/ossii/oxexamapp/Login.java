package com.ossii.oxexamapp;

import static com.ossii.oxexamapp.DbConstants.P_DESC;
import static com.ossii.oxexamapp.DbConstants.P_DURATION;
import static com.ossii.oxexamapp.DbConstants.P_END_AT;
import static com.ossii.oxexamapp.DbConstants.P_EXPIRE;
import static com.ossii.oxexamapp.DbConstants.P_MYSQL_EXAM_ID;
import static com.ossii.oxexamapp.DbConstants.P_MYSQL_PAPER_ID;
import static com.ossii.oxexamapp.DbConstants.P_NAME;
import static com.ossii.oxexamapp.DbConstants.P_NUM_OF_TIMES;
import static com.ossii.oxexamapp.DbConstants.P_PARTS;
import static com.ossii.oxexamapp.DbConstants.P_START_AT;
import static com.ossii.oxexamapp.DbConstants.P_SUBJECT_ID;
import static com.ossii.oxexamapp.DbConstants.SERVER_ADD;
import static com.ossii.oxexamapp.DbConstants.TABLE_NAME;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.beardedhen.androidbootstrap.BootstrapButton;
//import com.beardedhen.androidbootstrap.FontAwesomeText;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
//import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
//import android.view.View;
import android.webkit.WebView;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity {
	private WebView WebView = null;
	
	private DBHelper dbhelper = null;
	// UI執行序用的 (跟介面有關的)
	private Handler mUI_Handler = new Handler();
	private HandlerThread mThread;
	// 繁重執行序用的 (時間超過3秒的)
	private Handler mThreadHandler;
	
	@SuppressWarnings("deprecation")
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		//font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf" );
		// 這種執行序可以有名字!
		mThread = new HandlerThread("net");
		mThread.start();
		mThreadHandler = new Handler(mThread.getLooper());
		mThreadHandler.post(new Runnable() {
		@Override
		public void run() {
			// TODO 自動產生的方法 Stub
			final String jsonString = executeQuery("society");
			mUI_Handler.post(new Runnable() {

				@Override
				public void run() {
					// TODO 自動產生的方法 Stub
					openDatabase();
					saveDB(jsonString); //將mysql's json save to sqlite
					
					/** 服務相關 */
					//Intent intent = new Intent(Login.this,UpdateService.class);
				    //startService(intent); //啟動服務
		    		//stopService(intent); //關閉服務
				}
			});
		}
	});
		
		findViews();
		//addListenerOnButton();
		
		final JavaScriptInterface myJavaScriptInterface = new JavaScriptInterface(this);
		WebView.getSettings().setLightTouchEnabled(true);
		WebView.getSettings().setJavaScriptEnabled(true);
		WebView.addJavascriptInterface(myJavaScriptInterface, "AndroidFunction");
		WebView.loadUrl("file:///android_asset/www/login.html");
		
	}
	
	private void findViews(){
		WebView = (WebView)findViewById(R.id.loginpage);
	}
	
	public class JavaScriptInterface {
		Context mContext;
	    JavaScriptInterface(Context c) {
	        mContext = c;
	    }
	    public void showToast(String webMessage){
	    	Toast.makeText(mContext, webMessage, Toast.LENGTH_SHORT).show();
	    }
	    public void checkUaccount(String Uaccount, String Upassword){
	    	Toast.makeText(mContext, Uaccount+Upassword, Toast.LENGTH_SHORT).show();
	    	Intent intent = new Intent();
    		intent.setClass(Login.this, Test_list.class);
    		startActivity(intent);
	    }
    }

	/** 執行sql指令 */
	private String executeQuery(String query) {
		String result = "";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost post = new HttpPost(
					SERVER_ADD + "papers.php");
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("category", query));
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse httpResponse = httpClient.execute(post);
			HttpEntity httpEntity = httpResponse.getEntity();
			InputStream inputStream = httpEntity.getContent();
			BufferedReader bufReader = new BufferedReader(
					new InputStreamReader(inputStream, "utf-8"), 8);
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = bufReader.readLine()) != null) {
				builder.append(line + "\n");
			}
			inputStream.close();
			result = builder.toString();
		} catch (Exception e) {
			Log.e("log_tag", e.toString());
		}
		return result;
	}
	
	
	
	
	
	/** open database */
	private void openDatabase(){
		dbhelper = new DBHelper(this); 
	}
	
	/** save mysql's json data to sqlDB */
	public final void saveDB(String input) {
		SQLiteDatabase db = dbhelper.getWritableDatabase();
    	ContentValues values = new ContentValues();
		/**
		 * SQL 結果有多筆資料時使用JSONArray 只有一筆資料時直接建立JSONObject物件 JSONObject jsonData =
		 * new JSONObject(result);
		 */
		try {
			JSONArray jsonArray = new JSONArray(input);
			
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonData = jsonArray.getJSONObject(i);	//每張paper
				
				values.put(P_NUM_OF_TIMES, jsonData.getString("num_of_times"));
		    	values.put(P_START_AT, jsonData.getString("start_at"));
		    	values.put(P_NAME, jsonData.getString("name"));
		    	values.put(P_MYSQL_PAPER_ID, jsonData.getString("mysql_paper_id"));
		    	values.put(P_MYSQL_EXAM_ID, jsonData.getString("mysql_exam_id"));
		    	values.put(P_SUBJECT_ID, jsonData.getString("subject_id"));
		    	values.put(P_END_AT, jsonData.getString("end_at"));
		    	values.put(P_PARTS, jsonData.getString("parts"));
		    	values.put(P_DURATION, jsonData.getString("duration"));
		    	values.put(P_DESC, jsonData.getString("desc"));
		    	values.put(P_EXPIRE, jsonData.getString("expire"));
		    	
		    	int checkupdate = db.update(TABLE_NAME, values, P_MYSQL_EXAM_ID + "=" + jsonData.getString("mysql_exam_id"), null);
		    	if ( checkupdate == 0 ){
		    		db.insert(TABLE_NAME, null, values);
		    	}
			}
		} catch (JSONException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		}
	}
}
