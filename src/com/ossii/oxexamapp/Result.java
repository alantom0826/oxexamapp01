package com.ossii.oxexamapp;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Result extends Activity{
	
	private Button button_back;
	private TextView view_result;
	private TextView view_score;
	private ListView listView;
	private ArrayAdapter<String> listAdapter;
	List<String> list = new ArrayList<String>();
	private DBHelper dbhelper = null;
	private Cursor mCursor;
	private Bundle bundle;
    private JSONArray jsonArray;
    private int sum;
    //private ArrayList<ArrayList<ArrayList<ArrayList<String>>>> partsList = new ArrayList<ArrayList<ArrayList<ArrayList<String>>>>(); // allList
    //private ArrayList<String> myansList = new ArrayList<String>(); // myansList
    //private ArrayList<ArrayList<String>> subquestionsList = new ArrayList<ArrayList<String>>(); // subquestionsList
    //private ArrayList<ArrayList<ArrayList<String>>> questionsList = new ArrayList<ArrayList<ArrayList<String>>>(); // questionsList
    
    private ArrayList<String> myansList = new ArrayList<String>(); // myansList
    private ArrayList<String> okansList = new ArrayList<String>(); // okansList
    
	/** Called when the activity id first created. */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.result);
		findViews();
		showResults();
		openDatabase();
		calcSorce();
		getpaperdata();
		setListensers();
	}

	private void findViews(){
		button_back = (Button)findViewById(R.id.report_back);
		view_result = (TextView)findViewById(R.id.result);
		view_score = (TextView)findViewById(R.id.score);
		listView = (ListView)findViewById(R.id.listView);
	}
	
	// Listen for button clicks
	private void setListensers(){
		button_back.setOnClickListener(backMain);
	}
	private Button.OnClickListener backMain =  new Button.OnClickListener(){
		public void onClick(View v){
			// Close this Activity
			Result.this.finish();
		}
	};
	
	private void showResults(){
		bundle = this.getIntent().getExtras();
		for (int i=0; i<Integer.parseInt(bundle.getString("MUCH_QUESTIONS")); i++){
			if (bundle.getString("TEST" + String.valueOf(i)) == null){
				bundle.putString("TEST"+ String.valueOf(i), "0");
				list.add(bundle.getString("TEST" + String.valueOf(i)));
			}else{
				list.add(bundle.getString("TEST" + String.valueOf(i)));
			}
		}
		listAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,list);
		listView.setAdapter(listAdapter);
		//Present result
		//view_result.setText(bundle.getString("MUCH_QUESTIONS"));
		//view_result.setText(bundle.getString("MYSQL_EXAM_ID"));
	}
	
	private void calcSorce(){
		
		myansList.add("1");
		myansList.set(0,myansList.get(0).toString() + "-" + "1-2-234");
		myansList.add("2");
		view_score.setText(myansList.get(0).substring(2, 3));
		view_score.setText(myansList.get(0).substring(6));
		//view_suggest.setText(myansList.get(0).toString());
		//view_suggest.setText(myansList.toString());
		view_score.setText("100分");
		

	}
	
	private void addmyanslist(){
		
	}
	private void addokanslist(){
		
	}
	
	private void getpaperdata(){
		mCursor = dbhelper.getPaper4examid(Long.parseLong(bundle.getString("MYSQL_EXAM_ID")));
		mCursor.moveToFirst();
		JsonFactory(mCursor.getString(0));
		try {
		view_result.setText(jsonArray.optJSONArray(0).optJSONObject(0).optJSONArray("questions").optJSONObject(0).getString("topic"));
		//view_result.setText(jsonArray.optJSONArray(0).optJSONObject(0).optJSONArray("questions").optJSONObject(0).optJSONArray("ans").optString(0));
		view_result.setText(String.valueOf(jsonArray.optJSONArray(0).optJSONObject(0).optJSONArray("questions").optJSONObject(0).optJSONArray("ans").length()));
		//view_result.setText(String.valueOf(mCursor.getColumnCount()));
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/** open database */
	private void openDatabase(){
		dbhelper = new DBHelper(this); 
	}
	
	/** 處理json資料變為 jsonArray */
	public final void JsonFactory(String input){
		input = input.replace("&#039;", "'");
		input = input.replace("<br\\/>", "");
		try {
			jsonArray = new JSONArray(input);
		} catch (JSONException e) {
			// TODO 自動產生的 catch 區塊
			e.printStackTrace();
		}
	}
}

