package com.ossii.oxexamapp;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;
import static com.ossii.oxexamapp.DbConstants.P_DESC;
import static com.ossii.oxexamapp.DbConstants.P_ID;
import static com.ossii.oxexamapp.DbConstants.P_MYSQL_EXAM_ID;
import static com.ossii.oxexamapp.DbConstants.P_NAME;

public class Test_list extends Activity {
	
	private WebView WebView = null;
	private DBHelper dbhelper = null;
	private JSONArray jsonArrayPaperName;
    private Cursor CursorPaperName, CursorPaperDesc;
    private Long mRowId;
	
	@SuppressLint("SetJavaScriptEnabled")
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.test_list);
		
        findViews();
        openDatabase();
        Papername_Select_vsqlite();	  // 選擇考卷名稱 use sqlite
        Paperdesc_Select_vsqlite();	  // 選擇考卷desc use sqlite
        
		final JavaScriptInterface myJavaScriptInterface = new JavaScriptInterface(this);
		WebView.getSettings().setLightTouchEnabled(true);
		WebView.getSettings().setJavaScriptEnabled(true);
		WebView.addJavascriptInterface(myJavaScriptInterface, "AndroidFunction");
		WebView.loadUrl("file:///android_asset/www/test_list.html");
	}
	
	private void findViews(){
		WebView = (WebView)findViewById(R.id.testlistpage);
	}
	
	/** open database */
	private void openDatabase(){
		dbhelper = new DBHelper(this);
		dbhelper.open();
	}
	
	/** 處理json資料變為 jsonArrayPaperName */
	public final void JsonFactoryPaperName(String input){
		try {
			jsonArrayPaperName = new JSONArray(input);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/** 選擇考卷名稱 vsqlite*/	
	public final void Papername_Select_vsqlite(){
		CursorPaperName = dbhelper.getAllPapername();

		if(CursorPaperName!=null && CursorPaperName.getCount()>0){
			CursorPaperName.moveToFirst();
			if (CursorPaperName.isNull(0))
			{
				Log.d("CursorPaperNameData", "null");
			}
		}
	}
	
	/** 選擇考卷desc vsqlite*/	
	public final void Paperdesc_Select_vsqlite(){
		CursorPaperDesc = dbhelper.getAllPaperdesc();

		if(CursorPaperDesc!=null && CursorPaperDesc.getCount()>0){
			CursorPaperDesc.moveToFirst();
			if (CursorPaperDesc.isNull(0))
			{
				Log.d("CursorPaperDescData", "null");
			}
		}
	}
	
	public class JavaScriptInterface {
		Context mContext;
	    JavaScriptInterface(Context c) {
	        mContext = c;
	    }
	    
	    public void showToast(String webMessage){
	    	Toast.makeText(mContext, webMessage, Toast.LENGTH_SHORT).show();
	    }
	    
	    public void entertest(int num){
	    	Intent intent = new Intent();
	    	Bundle bundle = new Bundle();
    		intent.setClass(Test_list.this, MainActivity.class);
	    	bundle.putString("MYSQL_EXAM_ID", String.valueOf(num+1));
    		intent.putExtras(bundle);
    		startActivity(intent);
	    }
	    
	    public String getpapername(int num){
	    	CursorPaperName.moveToFirst();
	    	CursorPaperName.moveToPosition(num);
	    	return CursorPaperName.getString(0);
	    }
	    
	    public String getpaperdesc(int num){
	    	CursorPaperDesc.moveToFirst();
	    	CursorPaperDesc.moveToPosition(num);
	    	return CursorPaperDesc.getString(0);
	    }
	    
	    public String getpapercount(){
	    	return String.valueOf(CursorPaperName.getCount());
	    }
    }
}
