package com.ossii.oxexamapp;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private Bundle bundle;
	private WebView WebViewTopic = null;
	private TextView TextViewtop, TextViewBottom  = null;
	private Button previousbtn, nextbtn = null;
	private DBHelper dbhelper = null;
	private JSONArray jsonArrayParts;
    private Cursor CursorParts;
    private Long mRowId;
    private int iii = 1;
	
	@SuppressLint("SetJavaScriptEnabled")
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
        findViews();
        addListenerOnButton();
        openDatabase();
        //mRowId = (long)5;
        bundle = this.getIntent().getExtras();
        mRowId = Long.parseLong(bundle.getString("MYSQL_EXAM_ID"));
        //Toast.makeText(this, bundle.getString("MYSQL_EXAM_ID"), Toast.LENGTH_SHORT).show();
        Papar_Select_vsqlite(mRowId); // 選擇考卷 use sqlite
        
		final JavaScriptInterface myJavaScriptInterface = new JavaScriptInterface(this);
		//WebSettings websettings = WebViewTopic.getSettings();
		WebViewTopic.getSettings().setLightTouchEnabled(true);
		WebViewTopic.getSettings().setJavaScriptEnabled(true);
		//WebViewTopic.getSettings().setPluginState(WebSettings.PluginState.ON);
		WebViewTopic.setWebChromeClient(new WebChromeClient() {
			@Override
			public boolean onJsAlert(WebView view, String url, String message,JsResult result) {
			//Required functionality here
			return super.onJsAlert(view, url, message, result);
			 }
			});
		WebViewTopic.addJavascriptInterface(myJavaScriptInterface, "AndroidFunction");
		WebViewTopic.loadUrl("file:///android_asset/www/index03.html");
		//WebViewTopic.loadDataWithBaseURL("file:///android_res/raw/", readTextFromResource(R.raw.index02), "text/html", "UTF-8", null);
	}
	
	/** Listen for result button clicks */
	private void addListenerOnButton(){
		nextbtn.setOnClickListener(nextquestion);
		previousbtn.setOnClickListener(prequestion);
	}
	
	private Button.OnClickListener nextquestion = new Button.OnClickListener(){
		public void onClick(View v){
			iii++;
			//调用javascript的函数get4Android(str)
			WebViewTopic.loadUrl("javascript:nextbtn('"+ iii +"')");
		}
	};
	
	private Button.OnClickListener prequestion = new Button.OnClickListener(){
		public void onClick(View v){
			if (iii <= 1){
				iii = 1;
			}else{
				--iii;
			}
			//调用javascript的函数get4Android(str)
			WebViewTopic.loadUrl("javascript:previousbtn('"+ iii +"')");
		}
	};
	
	private void findViews(){
		WebViewTopic = (WebView)findViewById(R.id.testpagetopic);
		TextViewtop = (TextView)findViewById(R.id.testpagetop);
		TextViewBottom = (TextView)findViewById(R.id.testpagebottom);
		previousbtn = (Button) findViewById(R.id.prebutton);
		nextbtn = (Button) findViewById(R.id.nextbutton);
	}
	
	/**
     * 從raw 讀取 raw file，以支援多國語系
     */
    private String readTextFromResource(int resourceID) {
        InputStream raw = getResources().openRawResource(resourceID);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        int i;
        try {
            i = raw.read();
            while (i != -1) {
                stream.write(i);
                i = raw.read();
            }
            raw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream.toString();
    }
	
	/** open database */
	private void openDatabase(){
		dbhelper = new DBHelper(this);
		dbhelper.open();
	}
	
	/** 處理json資料變為 jsonArrayParts */
	public final void JsonFactoryParts(String input){
		//input = input.replace("&#039;", "'");
		//input = input.replace("<br\\/>", "");
		input = input.replace("{{q}}", "____");
		try {
			jsonArrayParts = new JSONArray(input);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/** 選擇考卷 vsqlite*/	
	public final void Papar_Select_vsqlite(Long exam_id){
		CursorParts = dbhelper.getPaper4id(exam_id);
		
		if(CursorParts!=null && CursorParts.getCount()>0){
			CursorParts.moveToFirst();
			if (CursorParts.isNull(0))
			{
				Log.d("CursorPartsData", "null");
			}
		}
	}
	
	public class JavaScriptInterface {
		Context mContext;
	    JavaScriptInterface(Context c) {
	        mContext = c;
	    }
	    public void showToast(String webMessage){
	    	Toast.makeText(mContext, webMessage, Toast.LENGTH_SHORT).show();
	    }
	    
	    public JSONArray getpaperparts(){
	    	JsonFactoryParts(CursorParts.getString(0));
	    	return jsonArrayParts;
	    }
	    
	    public int checkqtype(int part, int question_num){
	    	int type = 0;
	    	if (jsonArrayParts.optJSONArray(part).optJSONObject(0).optJSONArray("questions").optJSONObject(question_num).isNull("opt_ans_desc")){
	    		type = 2;
	    	}else{
	    		if(jsonArrayParts.optJSONArray(part).optJSONObject(0).optJSONArray("questions").optJSONObject(question_num).isNull("sub_topic")){
	    			int alen = jsonArrayParts.optJSONArray(part).optJSONObject(0).optJSONArray("questions").optJSONObject(question_num).optJSONArray("ans").length();
		    		if (alen > 1){
		    			type = 3;
		    		}else{
		    			int blen = jsonArrayParts.optJSONArray(part).optJSONObject(0).optJSONArray("questions").optJSONObject(question_num).optJSONArray("opt_ans_desc").length();
		    			if (blen == 2){
		    				type =4;
		    			}else{
		    				type = 1;
		    			}
		    		}
	    		}else{
		    		type = 5;
	    		}
	    	}
	    	return type;
	    }
	    
    }
	
}