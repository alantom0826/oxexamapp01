package com.ossii.oxexamapp;

import static com.ossii.oxexamapp.DbConstants.P_DESC;
import static com.ossii.oxexamapp.DbConstants.P_DURATION;
import static com.ossii.oxexamapp.DbConstants.P_END_AT;
import static com.ossii.oxexamapp.DbConstants.P_EXPIRE;
import static com.ossii.oxexamapp.DbConstants.P_ID;
import static com.ossii.oxexamapp.DbConstants.P_MYSQL_EXAM_ID;
import static com.ossii.oxexamapp.DbConstants.P_MYSQL_PAPER_ID;
import static com.ossii.oxexamapp.DbConstants.P_NAME;
import static com.ossii.oxexamapp.DbConstants.P_NUM_OF_TIMES;
import static com.ossii.oxexamapp.DbConstants.P_PARTS;
import static com.ossii.oxexamapp.DbConstants.P_START_AT;
import static com.ossii.oxexamapp.DbConstants.P_SUBJECT_ID;
import static com.ossii.oxexamapp.DbConstants.TABLE_NAME;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	
	private final static String DATABASE_NAME = "oxexam.db";
	private final static int DATABASE_VERSION = 1;
	
	/** Constructor */
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.mCtx = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		final String INIT_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
								  P_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
								  P_NUM_OF_TIMES + " INTEGER, " +
								  P_START_AT + " TEXT, " +
								  P_NAME + " TEXT, " +
								  P_MYSQL_PAPER_ID + " INTEGER, " +
								  P_MYSQL_EXAM_ID + " INTEGER, " +
								  //P_MYSQL_EXAM_ID + " INTEGER PRIMARY KEY, " +
								  P_SUBJECT_ID + " INTEGER, " +
								  P_END_AT + " TEXT, " +
								  P_PARTS + " TEXT, " +
								  P_DURATION + " INTEGER, " +
								  P_DESC + " INTEGER, " +
								  P_EXPIRE + " INTEGER);"; 
		db.execSQL(INIT_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
		db.execSQL(DROP_TABLE);
		onCreate(db);
	}
	
	private Context mCtx = null;
	private DBHelper dbHelper ;
	private SQLiteDatabase db;

	public DBHelper open () throws SQLException {
	    dbHelper = new DBHelper(mCtx);
	    db = dbHelper.getWritableDatabase();
	    return this;
	}

	public void close() {
	    dbHelper.close();
	}
	
	public Cursor getPaper4id(Long _id){
		SQLiteDatabase db=this.getReadableDatabase();
		return db.query(TABLE_NAME,
				new String[] {P_PARTS}, "_id=" + _id, null, null, null, null);
	}
	
	public Cursor getExamid(Long _id){
		SQLiteDatabase db=this.getReadableDatabase();
		return db.query(TABLE_NAME,
				new String[] {P_MYSQL_EXAM_ID}, "_id=" + _id, null, null, null, null);
	}
	
	public Cursor getAllPapername(){
		SQLiteDatabase db=this.getReadableDatabase();
		return db.query(TABLE_NAME,
				new String[] {P_NAME}, null, null, null, null, null);
	}
	
	public Cursor getAllPaperdesc(){
		SQLiteDatabase db=this.getReadableDatabase();
		return db.query(TABLE_NAME,
				new String[] {P_DESC}, null, null, null, null, null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * old functioin
	 * 
	 */
	
	public Cursor getPaper4examid(Long exam_id){
		SQLiteDatabase db=this.getReadableDatabase();
		return db.query(TABLE_NAME,
				new String[] {P_PARTS}, "mysql_exam_id=" + exam_id, null, null, null, null);
	}
	
	public Cursor getAll(){
		SQLiteDatabase db=this.getReadableDatabase();
		//return db.rawQuery("SELECT * FROM papers", null);
		return db.query(TABLE_NAME,
				null, null, null, null, null, null);
	}

}
