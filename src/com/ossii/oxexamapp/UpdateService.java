package com.ossii.oxexamapp;

import java.util.Date;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

public class UpdateService extends Service{
	
	private Handler handler = new Handler();
	  
	private Runnable showTime = new Runnable() {
		public void run() {
		// log目前時間
		Log.i("time:", new Date().toString());
		handler.postDelayed(this, 1000);
		}
	};
	
	@Override
	public IBinder onBind(Intent intent){
		return null;
	}
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Log.i("服務", "建立");
	}
	 
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i("服務", "銷毀");
		handler.removeCallbacks(showTime);
	}
	 
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.i("服務", "執行");
		handler.postDelayed(showTime, 1000);
		return super.onStartCommand(intent, flags, startId);
	}

}
